#include <iostream>
#include <vector>

int main( ) {
  std::vector<int> g1;

  for ( int i = 1; i <= 10; ++i ) {
    g1.push_back(i * 10);
  }

  std::cout << "Output of begin and end: ";
  for ( auto i = g1.begin(); i != g1.end(); ++i ) {
    std::cout << *i << " ";
  }

  std::cout << "\nOutput of cbegin and cend: ";
  for ( auto i = g1.cbegin(); i != g1.cend(); ++i ) {
    std::cout << *i << " ";
  }

  std::cout << "\nOutput of rbegin and rend: ";
  for ( auto i = g1.rbegin(); i != g1.rend(); ++i ) {
    std::cout << *i << " ";
  }

  std::cout << "\nOutput of crbegin and crend: ";
  for ( auto i = g1.crbegin(); i != g1.crend(); ++i ) {
    std::cout << *i << " ";
  }

  std::cout << "\nSize: " << g1.size();
  std::cout << "\nCapacity: " << g1.capacity();
  std::cout << "\nMax_size: " << g1.max_size();

  g1.resize( 4 );
  std::cout << "\nSize: " << g1.size();

  if ( g1.empty() )
    std::cout << "\nVector is empty";
  else
    std::cout << "\nVector is not empty";

  g1.shrink_to_fit();
  std::cout << "\nVector elements are: ";
  for ( auto it = g1.begin(); it != g1.end(); ++it ) {
    std::cout << *it << " ";
  }

  std::cout << "\nReference operator [g] : g1[2] = " << g1[2];
  std::cout << "\nat :: g1.at( 3 ) = " << g1.at( 3 );
  std::cout << "\nfront() : g1.front() = " << g1.front();
  std::cout << "\nback() : g1.back() = " << g1.back();

  int* pos = g1.data();
  std::cout << "\nThe first element is " << *pos;

  return 0;
}
