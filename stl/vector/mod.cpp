#include <cstdio>
#include <vector>

int main() {
  std::vector<int> v;
  v.assign(5, 10); // fill v with five 10
  printf( "The vector elements are: " );
  for ( auto element = v.begin(); element != v.end(); ++element ) {
     printf( "%d ", *element );
  }

  v.push_back( 15 );
  int n = v.size();
  printf( "\nThe last elements is: %d", v[n - 1] );
  v.pop_back();

  printf( "\nThe vector elements are: " );
  for ( auto element = v.begin(); element != v.end(); ++element ) {
     printf( "%d ", *element );
  }

  v.insert( v.begin(), 5 );
  printf( "\nThe first element is %d", v[0] );

  v.erase( v.begin() );
  printf( "\nThe first element is %d", v[0] );

  v.emplace( v.begin(), 5 );
  printf( "\nThe first element is %d", v[0] );

  v.emplace_back( 20 );
  n = v.size();
  printf( "\nThe first last element is: %d", v[n - 1] );

  v.clear();
  printf( "\nVector size after erase(): %d", v.size() );



  return 0;
}
