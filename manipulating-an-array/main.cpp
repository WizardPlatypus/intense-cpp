#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using uint = unsigned int;
using ulong = unsigned long;

class Operation {
  public: uint begin{}, end{}, summand{};
};

bool Conqueror( Operation a, Operation b ) {
  return a.end < b.end;
}

ulong setManipulation( std::vector<Operation> operations ) {
  // + O(m*log(m))
  std::sort( operations.begin(), operations.end(), Conqueror );
  uint The_End = operations.back().end;

  uint begin{0};
  ulong max { std::numeric_limits<ulong>::min() };
  uint last_number { 0 };
  for ( uint i{0}; i < operations.size(); ++i ) { // + O(m) * ...
    uint number { operations[i].begin };
    if ( number == last_number ) { // const time
      last_number = number;
      continue;
    }
    ulong value { 0 };
    uint index { begin }; // So it's O(m) once for all of the iterations
    while ( number > operations[index].end ) {
       ++index;
    }
    begin = index;
    // have no idea how long it actually takes
    // surely less than O(m)
    // and it gets better with every iteration
    // and it aims to 0
    // i guess it's O(m / 2)
    // but i'm not a scientist
    for ( ; index < operations.back().end; ++index ) {
      value += operations[index].summand;
    }
    if ( value > max ) { // const time
      max = value;
    }
    last_number = number;
  }
}

int main() {
  
}
