#include <iostream>
#include <vector>
#include <limits>

using uint = unsigned int;
using ulong = unsigned long;

struct Operation {
  uint begin{}, end{}, summand{};
  bool appliesTo( size_t index ) const {
    return index >= begin && index <= end;
  }
};

ulong setManipulation( size_t set_size,
      std::vector<Operation> operations ) {
  ulong max { std::numeric_limits<ulong>::min() };
  for ( size_t index{ 0 }; index < set_size; ++index ) {
    ulong value{0};
    for ( const auto& operation: operations ) {
      if ( operation.appliesTo( index + 1 ) ) {
        value += operation.summand;
      }
    }
    if ( value > max ) {
      max = value;
    }
  }
  return max;
}

int main() {
  size_t set_size{}, operations_count{};

  std::cin >> set_size >> operations_count;
  std::vector<Operation> operations(operations_count);
  for ( auto& operation: operations ) {
    std::cin
      >> operation.begin
      >> operation.end
      >> operation.summand;
  }

  auto result = setManipulation( set_size, operations );
  std::cout << result << std::endl;
  return 0;
}
