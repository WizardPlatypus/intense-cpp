#include <vector>

class Vertex {
  int label;
  std::vector<Edge> edges;
};

class Edge {
  int label;
  Vertex* begin;
  Vertex* end;
};

class Graph {
  std::vector<Vertex> V;
  std::vector<Edge> E;
};


