#include <cstdio>

int absolute_value( int x ) {
  if ( x >= 0 )
    return x;
  else
    return x * -1;
}

int sum( int a, int b ) {
  return a + b;
}

int main( ) {
  int my_num = 0;
  printf( "The absolute value of %d is %d.\n", my_num,
      absolute_value( my_num ) );
  int my_second_num = 4;
  printf( "The sum of %d and %d is %d.",
      my_num, my_second_num,
      sum( my_num, my_second_num ) );
  return 0;
}
